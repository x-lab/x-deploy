use std::os::raw::c_char;

use qmetaobject::prelude::*;

use x_logger;
use x_gui;
use x_quick;
#[cfg(feature = "tts")]
use x_tts;
#[cfg(feature = "vis")]
use x_vis;

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

qrc!(register_resources, "/" {
    "src/main.qml",
});

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

#[no_mangle]
fn x_deply_rinit() {
    x_logger::init();
     x_quick::init();
#[cfg(feature = "tts")]
       x_tts::init();
#[cfg(feature = "vis")]
       x_vis::init();
}

#[no_mangle]
fn x_deply_rmain() {

    register_resources();

    let mut engine = x_gui::xApplicationEngine::new();
    engine.set_application_name("x-deploy Test".into());
    engine.set_organisation_name("inria".into());
    engine.set_organisation_domain("fr".into());
    engine.add_import_path("qrc:///qml".into());
    engine.add_import_path("qrc:///src".into());
    engine.set_property("_title".into(), engine.title().into());
    engine.set_source("qrc:///src/main.qml".into());
    engine.resize(1024, 600);
    engine.exec();
}

// /////////////////////////////////////////////////////////////////////////////
// For the record
// /////////////////////////////////////////////////////////////////////////////

#[no_mangle]
pub extern "C" fn x_deply_rfunc(name: *const c_char) {
    let name = unsafe { std::ffi::CStr::from_ptr(name).to_str().unwrap() };
    println!("Hello, {}! I'm Rust!", name);
}
