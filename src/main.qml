import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import xLogger         as L

import xQuick          as X
import xQuick.Controls as X
import xQuick.Style    as X

X.Application {

    id: window;

    X.LabelHeadline3 {
        anchors.centerIn: parent;

        text: "Gotcha!";
    }
}
