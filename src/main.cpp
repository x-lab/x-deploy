#include <QtCore>
#include <QtDebug>

extern "C" void x_deply_rfunc(const char*);
extern "C" void x_deply_rinit();
extern "C" void x_deply_rmain();

int main(int argc, char *argv[])
{
    x_deply_rinit();

    if (argc < 2)
        x_deply_rfunc("You");
    else
        x_deply_rfunc(argv[1]);

    qInfo() << "Test";

    x_deply_rmain();

    return 0;
}
